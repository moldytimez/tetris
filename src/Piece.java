package tetris;

import java.awt.*;
import java.awt.Color;
import java.awt.Graphics;

/**
 * Created by Yiduo Jing on 9/24/16.
 */

class Piece {

    // all possible combinations of points to make the pieces
    private Point[][][] possPieces = {
            // I 0
            {
                    { new Point(1,0), new Point(1,1), new Point(1,2), new Point(1,3) }, // initial
                    // rotate right
                    { new Point(0,1), new Point(1,1), new Point(2,1), new Point(3,1) }, // rotate once
                    { new Point(2,0), new Point(2,1), new Point(2,2), new Point(2,3) }, // rotate twice
                    { new Point(0,2), new Point(1,2), new Point(2,2), new Point(3,2) }, // rotate three times
                    // rotate left
                    { new Point(0,2), new Point(1,2), new Point(2,2), new Point(3,2) }, // rotate once
                    { new Point(2,0), new Point(2,1), new Point(2,2), new Point(2,3) }, // rotate twice
                    { new Point(0,1), new Point(1,1), new Point(2,1), new Point(3,1) }  // rotate three times
            },
            // L 1
            {
                    { new Point(1,0), new Point(1,1), new Point(1,2), new Point(2,2) }, // initial
                    // rotate right
                    { new Point(1,1), new Point(2,1), new Point(3,1), new Point(1,2) }, // 1
                    { new Point(1,1), new Point(2,1), new Point(2,2), new Point(2,3) }, // 2
                    { new Point(2,1), new Point(0,2), new Point(1,2), new Point(2,2) }, // 3
                    // rotate left
                    { new Point(2,1), new Point(0,2), new Point(1,2), new Point(2,2) }, // 1
                    { new Point(1,1), new Point(2,1), new Point(2,2), new Point(2,3) }, // 2
                    { new Point(1,1), new Point(2,1), new Point(3,1), new Point(1,2) }, // 3
            },
            // J 2

            {
                    { new Point(2,0), new Point(2,1), new Point(1,2), new Point(2,2) }, // initial

                    // rotate right
                    { new Point(1,1), new Point(1,2), new Point(2,2), new Point(3,2) }, // 1
                    { new Point(1,1), new Point(2,1), new Point(1,2), new Point(1,3) }, // 2
                    { new Point(0,1), new Point(1,1), new Point(2,1), new Point(2,2) }, // 3
                    // rotate left
                    { new Point(0,1), new Point(1,1), new Point(2,1), new Point(2,2) }, // 1
                    { new Point(1,1), new Point(2,1), new Point(1,2), new Point(1,3) }, // 2
                    { new Point(1,1), new Point(1,2), new Point(2,2), new Point(3,2) }, // 3

            },

            // T 3
            {
                    { new Point(1,1), new Point(2,1), new Point(3,1), new Point(2,2) }, // initial

                    // rotate right
                    { new Point(2,1), new Point(1,2), new Point(2,2), new Point(2,3) }, // 1
                    { new Point(1,1), new Point(0,2), new Point(1,2), new Point(2,2) }, // 2
                    { new Point(1,0), new Point(1,1), new Point(2,1), new Point(1,2) }, // 3
                    // rotate left
                    { new Point(1,0), new Point(1,1), new Point(2,1), new Point(1,2) }, // 1
                    { new Point(1,1), new Point(0,2), new Point(1,2), new Point(2,2) }, // 2
                    { new Point(2,1), new Point(1,2), new Point(2,2), new Point(2,3) }, // 3
            },

            // O 4
            {
                    {new Point(1, 1), new Point(2, 1), new Point(1, 2), new Point(2, 2)}, // initial
                    {new Point(1, 1), new Point(2, 1), new Point(1, 2), new Point(2, 2)}, // 1
                    {new Point(1, 1), new Point(2, 1), new Point(1, 2), new Point(2, 2)}, // 2
                    {new Point(1, 1), new Point(2, 1), new Point(1, 2), new Point(2, 2)}, // 3

                    {new Point(1, 1), new Point(2, 1), new Point(1, 2), new Point(2, 2)}, // 1
                    {new Point(1, 1), new Point(2, 1), new Point(1, 2), new Point(2, 2)}, // 2
                    {new Point(1, 1), new Point(2, 1), new Point(1, 2), new Point(2, 2)}, // 3
            },
            // S 5
            {
                    {new Point(2, 1), new Point(3, 1), new Point(1, 2), new Point(2, 2)}, // initial

                    // rotate right
                    { new Point(1,1), new Point(1,2), new Point(2,2), new Point(2,3) }, // 1
                    { new Point(1,1), new Point(2,1), new Point(0,2), new Point(1,2) }, // 2
                    { new Point(1,0), new Point(1,1), new Point(2,1), new Point(2,2) }, // 3
                    // rotate left
                    { new Point(1,0), new Point(1,1), new Point(2,1), new Point(2,2) }, // 1
                    { new Point(1,1), new Point(2,1), new Point(0,2), new Point(1,2) }, // 2
                    { new Point(1,1), new Point(1,2), new Point(2,2), new Point(2,3) }, // 3
            },

            // Z 6
            {
                    {new Point(1, 1), new Point(2, 1), new Point(2, 2), new Point(3, 2)}, // initial

                    // rotate right
                    { new Point(2,1), new Point(1,2), new Point(2,2), new Point(1,3) }, // 1
                    { new Point(0,1), new Point(1,1), new Point(1,2), new Point(2,2) }, // 2
                    { new Point(2,0), new Point(1,1), new Point(2,1), new Point(1,2) }, // 3
                    // rotate left
                    { new Point(2,0), new Point(1,1), new Point(2,1), new Point(1,2) }, // 1
                    { new Point(0,1), new Point(1,1), new Point(1,2), new Point(2,2) }, // 2
                    { new Point(2,1), new Point(1,2), new Point(2,2), new Point(1,3) }, // 3
            },


    };
    private char shape; // Shape Character
    private int shapeN; // Shape number e.g 0 for I
    private int rotateMap = 0; // rotateMap for mapping the rotate num to the pieceArray elements

    public Point origin; // orgin for each piece for drawing piece
    public  Color c; // color of piece for drawing
    public int rotate = 0; // rotate number for piece; set this from rotate method

    // constructor
    public Piece(char shape)
    {
        this.shape = shape;
        this.origin = new Point(2,0);
        this.rotate = 0;
        this.rotateMap = 0;

        switch (this.shape) {
            case 'I': // I
                this.c = Color.cyan;
                this.shapeN = 0;
                break;
            case 'L': // L
                this.c = Color.decode("#FF4500").brighter();
                this.shapeN = 1;
                break;
            case 'J': // J
                this.c = Color.blue.darker();
                this.shapeN = 2;
                break;
            case 'T': // T
                this.c = Color.magenta.darker();
                this.shapeN = 3;
                break;
            case 'O': // O
                this.c = Color.yellow.brighter();
                this.shapeN = 4;
                break;
            case 'S': // S
                this.c = Color.green;
                this.shapeN = 5;
                break;
            case 'Z': // Z
                this.c = Color.red;
                this.shapeN = 6;
                break;
        }
    }

    public void SetOrigin(Point origin)
    {
        this.origin = origin;
    }

    // Set rotate to the rotateMap for accessing the right element in pieceArray
    public void SetRotate(int rotate)
    {
        this.rotate = rotate;
          this.rotateMap = rotate;

        switch (rotate) {
            case -1:
                this.rotateMap = 4;
                break;
            case -2:
                this.rotateMap = 5;
                break;
            case -3:
                this.rotateMap = 6;
                break;
        }
    }

    // Get shape character
    public char GetShape() { return this.shape; }

    boolean setNewPieceWell;
    // Draw Piece
    public void paint(Graphics g) {
        paintNextPiece(g,10*this.gridWid+20,NextPieceSize);
        if(!setNewPieceWell) return;
        for (Point p : possPieces[this.shapeN][this.rotateMap]) {
            g.setColor(this.c);
            g.fillRect((p.x + origin.x) * this.gridWid, (p.y + origin.y) * this.gridWid, this.gridWid, this.gridWid);
            g.setColor(Color.gray.brighter());
            g.drawRect((p.x + origin.x) * this.gridWid, (p.y + origin.y) * this.gridWid, this.gridWid, this.gridWid);
        }
    }

    int NextPieceSize;
    public void paintNextPiece(Graphics g, int originX, int originY) {

        for (Point p : possPieces[this.shapeN][this.rotateMap]) {
            g.setColor(this.c);
            g.fillRect(p.x * this.gridWid + originX, p.y * this.gridWid + originY, this.gridWid, this.gridWid);
            g.setColor(Color.gray.brighter());
            g.drawRect(p.x * this.gridWid + originX, p.y * this.gridWid + originY, this.gridWid, this.gridWid);
        }
    }


    // grid size set due to resize
    private  int gridWid;
    public void setGridWid(int wid)
    {
        this.gridWid = wid;

    }

    // Get the true points after adding origin
    public Point[] getPoints()
    {
        int pointN = 0;
        Point[] points = new Point[4];
        for(Point p: possPieces[this.shapeN][this.rotateMap]){
            points[pointN] = new Point(p.x + origin.x,p.y + origin.y);
            pointN++;
            if(pointN == 4) break;
        }
        return points;
    }


    // Get the points without adding origin
    public Point[] getShapePoints()
    {
        int pointN = 0;
        Point[] points = new Point[4];
        for(Point p: possPieces[this.shapeN][this.rotateMap]){
            points[pointN] = new Point(p.x,p.y);
            pointN++;
            if(pointN == 4) break;
        }
        return points;

    }


    // Get the color of piece
    public Color getColor()
    {
        return this.c;
    }

}
