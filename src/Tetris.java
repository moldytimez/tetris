package tetris;



import javax.swing.*;
import java.awt.*;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;
import javax.swing.Timer;


/**
 * Created by bwbecker on 2016-09-19.
 */

/**
 * Modified by Yiduo Jing on 2016-09-24.
 */

public class Tetris extends JComponent implements ActionListener {

    Vector<Piece> pendingPieces = new Vector<>();

    Piece currentP = null;

    Point origin = new Point(2,0);
    boolean hasSeqArg = false;
    int FPS = 30;
    double speed = 7.0; // update later
    private Timer timer;

    public Tetris(int fps, double speed, String sequence) {
        if(!sequence.isEmpty())
        {
            this.hasSeqArg = true;
            for (int i=0; i < sequence.length(); i++) {
                Piece p = new Piece(sequence.charAt(i));

                this.pendingPieces.add(p);
            }
        }
        this.FPS = fps;
        this.speed = speed;
    }


    Color[][] boardCovered = new Color[10][24];

    public boolean bGameStart = false;
    private int gameN = 0;
    // initialize the game start state
    public void tetrisInit()
    {
        currentP = null;
        this.bGameStart = true;
        this.gameN++;
        this.score = 0;

        this.origin = new Point(2,0);
        for(int i=0;i<10;i++)
        {
            for(int j=0;j<24;j++)
            {
                this.boardCovered[i][j] = Color.black;
            }
        }

        double dropTime = this.speed/20.0 * 1000.0; // update later
        this.timer =
                new Timer((int) dropTime, this);

        // init a new piece and make it drop down
        this.producePiece();

        this.timer.start();
    }


    public void actionPerformed(ActionEvent e) {
        if(!bNewPieceSetWell) return;
        boolean bIsPieceDropFinish = pieceDropFinish();

        if(!bIsPieceDropFinish) {
            natureDropDown();
        }
        else
        {
            bNewPieceSetWell = false;
            this.selectedPiece = null;
            this.selectedShapePoint = null;
            this.bPieceSelected = false;
            producePiece();
        }
    }

    // method for checkingTopOut
    public boolean newPieceSuccess(Piece p)
    {

        Point[] shapePoints = p.getShapePoints();

        // check 2
        for(int k=0;k<4;k++)
        {
            int row = shapePoints[k].x+this.origin.x;
            int col = shapePoints[k].y+this.origin.y;

            if(row <0 || row > 9 || col <0 || col > 23 ||
                    this.boardCovered[row][col] != Color.black)
            {
                return false;
            }
        }

            return true;
    }

    // method for new piece
    int pendingArgSeq = 0; //  index of pieces in the sequence
    public  boolean bNewPieceSetWell = false;
    char pieceC;
    public void producePiece()
    {
        this.origin = new Point(2,0);

        if(hasSeqArg)
        {
            if(this.pendingArgSeq >= this.pendingPieces.size()) {
                this.pendingArgSeq = 0;
            }
            this.currentP = new Piece(this.pendingPieces.elementAt(this.pendingArgSeq).GetShape());
            pieceC = this.currentP.GetShape();
            this.pendingArgSeq++;
        }
        else{
            // No argument passing for piece sequences
            //  Random choose piece 
            Random rand = new Random();
            int pieceN = rand.nextInt(7);

            if(pieceN == 0) { pieceC = 'I'; }
            else if(pieceN == 1) { pieceC = 'L'; }
            else if(pieceN == 2) { pieceC = 'J';}
            else if(pieceN == 3) { pieceC = 'T'; }
            else if(pieceN == 4) { pieceC = 'O';}
            else if(pieceN == 5) { pieceC = 'S';}
            else { pieceC = 'Z';}

            this.currentP = new Piece(pieceC);
        }

        if(this.currentP != null) {
            if (pieceC == 'T' ||
                    pieceC == 'O' ||
                    pieceC == 'S' ||
                    pieceC == 'Z')
                this.origin = new Point(2, -1);


            if (!newPieceSuccess(this.currentP)) {
                timer.stop();
                gameOver();
                return;
            }

            this.currentP.SetOrigin(this.origin);
            bNewPieceSetWell = true;
        }
    }


    boolean bLevel1Clear = false;

    boolean bLevel2Clear = false;
    boolean bGameOver = false;
    boolean bGame2Over = false;
    boolean bPreLevel1Clear = false;

    public void gameOver()
    {
        bGameStart = false;
        bGameOver = true;
     //   currentP = null;

        if(this.bLevel1Clear)
        {
            if(this.score >= 250)
                this.bLevel2Clear = true;
            bGame2Over = true;
        }
        else{
            if(this.score >= 100) {
                this.bLevel1Clear = true;
            }
        }

        repaint();

        if(bPreLevel1Clear == false && bLevel1Clear){
            bPreLevel1Clear = true;
            tetrisInit();
        }
    }


    // boolean for checking whether the piece is touching the ground
    // May clean later
    public boolean isTouchingGround()
    {
        Point[] points = this.currentP.getPoints();
        int maximumY = Math.max(Math.max(Math.max(points[0].y,points[1].y),points[2].y),points[3].y);

        if(maximumY == 23) {
            return true;
        }
        else
            return false;
    }


    // method for piece dropping down without any events
    public void natureDropDown()
    {
        if(!bNewPieceSetWell) return;
        this.origin.y += 1;
        this.currentP.SetOrigin(this.origin);

        repaint();

    }


    // check whether can move left and right
    // may clean later
    public boolean canMove(int direction) {
        if(currentP == null) return false;

        Point[] points = this.currentP.getPoints();

        // Whether touch left right edge
        int minimumX = Math.min(Math.min(Math.min(points[0].x,points[1].x),points[2].x),points[3].x);
        if(direction == -1 && minimumX == 0)
            return false;

        int maximumX = Math.max(Math.max(Math.max(points[0].x,points[1].x),points[2].x),points[3].x);
        if(direction == 1 && maximumX == 9)
            return false;

        return !whetherTouchOthers(direction,0);
    }


    // check whether moving left/right/drop touching other pieces
    public boolean whetherTouchOthers(int xDirection, int yDirection)
    {
        Point[] points = this.currentP.getPoints();

        // whether touch with other pieces
        // Test later
        for(int i=0;i<4;i++)
        {
            if(points[i].x+xDirection <0 || points[i].x+xDirection > 9 ||
                    points[i].y+yDirection <0 ||
                    points[i].y+yDirection> 23 ||
                    this.boardCovered[points[i].x+xDirection][points[i].y+yDirection]
                    != Color.black)
            {
                return true;
            }
        }

        return false;

    }

    // method for piece moving left
    public void moveLeft()
    {
        if(currentP == null) return;

        if(canMove(-1)) {
            origin.x -= 1;
            this.currentP.SetOrigin(origin);
            repaint();
        }

    }

    // method for piece moving right
    public void moveRight()
    {
        if(currentP == null) return;
        if(canMove(1)) {
            origin.x += 1;
            this.currentP.SetOrigin(origin);
            repaint();
        }

    }

    // method for piece dropping down or moving up without repaint
    public void keepUpOrDown(int direction) // 1 for down and -1 for up
    {
        if(currentP == null) return;
        this.origin.y += direction;
        this.currentP.SetOrigin(this.origin);
    }

    // method for piece dropping down by pressing space
    public void pieceDrop()
    {
        if(currentP == null) return;

        while(!isTouchingGround() && !whetherTouchOthers(0,1))
        {
            keepUpOrDown(1);
        }
        // May remove
        repaint();
    }


    // a upgrade method for just checking the temporary points (for rotate)
    public boolean whetherTouchOthersUpgrade(Point[] points)
    {
        // whether touch with other pieces
        // Test later
        for(int i=0;i<4;i++)
        {
            if(points[i].x <0 || points[i].x > 9 || points[i].y <0 || points[i].y > 23 ||
                    this.boardCovered[points[i].x][points[i].y]
                    != Color.black )
            {
                return true;
            }
        }

        return false;

    }

    public void rotate(int direction)
    {
        if(currentP == null) return;
        // record currentP's rotate;
        int prevRotate = this.currentP.rotate;

        Piece tempP = new Piece(currentP.GetShape());
        tempP.SetOrigin(currentP.origin);

        int rotate = (prevRotate + direction) % 4;
        tempP.SetRotate(rotate);

        Point[] tempPoints = tempP.getPoints();
        if(!whetherTouchOthersUpgrade(tempPoints))
        {
            currentP.SetRotate(tempP.rotate);
            repaint();
        }
    }


    // method for pausing
    boolean bIsPause = false;
    public void Pause()
    {
        // Need more tests later!!!!
        // resize!!!
        if(!this.bIsPause) {
            this.timer.stop();
            this.bIsPause = true;
        }
        else
        {
            this.timer.restart();
            this.bIsPause = false;
        }
        repaint();
    }

    // method for checking whether pieceDrop, clear line and scoring finish
    public boolean pieceDropFinish()
    {
        if(!bNewPieceSetWell) return false;
        if((!isTouchingGround() && !whetherTouchOthers(0,1)))
        {
            return false;
        }
        else
        {
            Point points[] = this.currentP.getPoints();
            for(int i=0;i<4;i++)
            {
                this.boardCovered[points[i].x][points[i].y] = this.currentP.getColor();
            }

            return deleteRowScoreFinish();
        }
    }

    // method for searching the cleared rows
    public int rowNeedsClear()
    {
        for(int j=23;j>=0;j--)
        {
            boolean bRowAllFill = true;
            for(int i=0;i<10;i++)
            {
                if(this.boardCovered[i][j] == Color.black){
                    bRowAllFill = false;
                    break;
                }
            }
            if(bRowAllFill)
            {
                return j;
            }
        }
        return -10;
    }

    // method for delete rows and score
    int score = 0;
    public boolean deleteRowScoreFinish()
    {
        int combo = 0;
        while(true)
        {
            int rowN = rowNeedsClear();
            if(rowN != -10)
            {
                reSetDrawBoard(rowN);
                combo++;
                score+=combo*5;
            }
            else
                break;
        }

        return true;
    }

    // reset the drawBoard with the row that is needed to be cleared
    void reSetDrawBoard(int row)
    {
        if(row == 0)
        {
            for(int i=0;i<10;i++)
            {
                this.boardCovered[i][row] = Color.black;
            }
        }
        else {
            for (int j = row; j >= 1; j--) {
                for (int i = 0; i < 10; i++) {
                    this.boardCovered[i][j] = this.boardCovered[i][j - 1];
                }
            }
        }
        repaint();
    }



    private Piece selectedPiece = null;
    private Point selectedShapePoint = null;

    boolean isSelected(Point p)
    {
        Point[] points = this.currentP.getPoints();
        for(int i=0;i<4;i++)
        {
            int x = (points[i].x)*this.gridWid;
            int y = (points[i].y)*this.gridWid;
            if((p.x >=x && p.x <= x+this.gridWid) && (p.y >=y && p.y <= y+this.gridWid))
            {
                this.selectedShapePoint = this.currentP.getShapePoints()[i];
                return true;
            }
        }

        return false;
    }


    private  boolean mousePressed = false;
    public boolean bPieceSelected = false;
    public void mousePressed(Point p)
    {
        if(currentP == null) return;
        this.mousePressed = true;
        // Test later
        if(isSelected(p)) {
            // select the current piece and the selected piece is null when before no piece is selected
            if (selectedPiece == null) {
                this.selectedPiece = this.currentP;
                this.bPieceSelected = true;
            }

            // select the current piece and the selected piece before is equal to current piece
            // make it drop down
            else if (this.selectedPiece == currentP) {
                pieceDrop();
                this.selectedPiece = null;
                this.bPieceSelected = false;
            }
        }

    }

    public void mouseMoved(Point p)
    {
        // More Tests!!!!
        if(this.selectedPiece != currentP || this.mousePressed){
            this.mousePressed = false;
            return;
        }

        Point movedPoint = null;
        for(int i=0;i<10;i++)
        {
            for(int j=0;j<24;j++)
            {
                int x = i*this.gridWid;
                int y = j*this.gridWid;
                if((p.x >=x && p.x <= x+this.gridWid) && (p.y >=y && p.y <= y+this.gridWid))
                {
                    if(boardCovered[i][j] != Color.black){
                        return;
                    }
                    else
                    {
                        movedPoint = new Point(i,j);
                        break;
                    }

                }
            }

            if(movedPoint != null)
                break;
        }

        if(movedPoint != null) {

            mouseMovePiece(movedPoint);
        }
    }



    public void mouseMovePiece(Point movedPoint)
    {
        if(currentP == null) return;

        if(selectedShapePoint == null)
            return;

        Point newOrigin = new Point(movedPoint.x - selectedShapePoint.x,
                origin.y);

        Piece tempP = new Piece(this.currentP.GetShape());
        tempP.SetOrigin(newOrigin);
        tempP.SetRotate(currentP.rotate);

        if(!whetherTouchOthersUpgrade(tempP.getPoints()))
        {
            this.origin = tempP.origin;
            currentP.SetOrigin(tempP.origin);
            repaint();
        }
    }


    public int gridWid = 28;
    public int PausedSize = 30;
    public int scoreSize = 30;
    public double prevWid = 530.0;
    public double prevH = 700.0;

    public void setCompSize(double width, double height)
    {

        int gWidFromW = (int) ((280.0/530.0 * width) / 10.0);
        int gWidFromH = (int) ((672.0/700.0 * height) / 24.0);

        if(height != prevH) {
            if (gWidFromH * 10 > width) {
                this.gridWid = gWidFromW;
            } else {
                this.gridWid = gWidFromH;
            }
        }

        else {

            if (gWidFromW * 24 > height) {
                this.gridWid = (int) (height / 24.0);
            } else
                this.gridWid = gWidFromW;

        }

        this.PausedSize = 30/28 * this.gridWid;
        this.scoreSize = 30/28 * this.gridWid;
        prevH = height;
        prevWid = width;

    }


    public void paintBoard(Graphics g)
    {
        for(int i=0;i<10;i++)
        {
            for(int j=0;j<24;j++)
            {
                if(this.boardCovered[i][j] != Color.black) {
                    g.setColor(this.boardCovered[i][j]);
                    g.fillRect(this.gridWid*i, this.gridWid*j, this.gridWid, this.gridWid);
                    g.setColor(Color.black.brighter());
                    g.drawRect(this.gridWid*i, this.gridWid*j, this.gridWid, this.gridWid);
                }
            }
        }
    }

    public void paintPaused(Graphics g)
    {
        if(bIsPause)
        {
            g.setFont(new Font("Arial",Font.BOLD,PausedSize));
            g.setColor(Color.black.brighter());
            g.drawString("PAUSED",10,330);
        }
    }


    boolean paint1Once = false;

    public void paintGame1Result(Graphics g) {
        if (bGameOver && !paint1Once) {
            if (!bLevel1Clear) {
                g.setFont(new Font("Arial", Font.BOLD, PausedSize));
                g.setColor(Color.black.darker());
                g.drawString("YOU LOSE", 10, 330);
            }
            paint1Once = true;
        }
    }

    boolean paint2Once = false;
    public void paintGame2Result(Graphics g)
    {
        if(bGame2Over && !paint2Once) {
            if ((!bLevel1Clear) || (!bLevel2Clear)) {
                g.setFont(new Font("Arial", Font.BOLD, PausedSize));
                g.setColor(Color.black.darker());
                g.drawString("YOU LOSE", 10, 330);
            }

            if (bLevel2Clear) {
                g.setFont(new Font("Arial", Font.BOLD, PausedSize));
                g.setColor(Color.black.brighter());
                g.drawString("YOU WIN", 10, 330);
            }
            paint2Once = true;
        }
    }



    public int game1StartPaint = 5;

    public void paintGame1Start(Graphics g)
    {
        if(this.game1StartPaint > 0){
            g.setFont(new Font("Arial", Font.BOLD, PausedSize));
            g.setColor(Color.black.brighter());
            g.drawString("LEVEL 1 START", 10, 230);
            g.drawString("GOAL 100", 30, 230+PausedSize);
            this.game1StartPaint--;
        }
    }


    public int game2StartPaint = 5;

    public void paintGame2Start(Graphics g)
    {
        if(this.game2StartPaint > 0){
            if (bLevel1Clear) {
                g.setFont(new Font("Arial", Font.BOLD, PausedSize));
                g.setColor(Color.black.brighter());
                g.drawString("LEVEL 2 START", 10, 230);
                g.drawString("GOAL 250", 30, 230+PausedSize);
                this.game2StartPaint--;
            }
        }
    }


    public void paintScoreBoard(Graphics g)
    {
        // score
        g.setFont(new Font("Arial",Font.BOLD,scoreSize));
        g.setColor(Color.YELLOW);
        g.drawString(Integer.toString(this.score),10*this.gridWid+20,scoreSize);

    }

    public void paintComponent(Graphics g)
    {
        g.setColor(Color.lightGray.brighter());
        g.fillRect(0, 0, 10 * this.gridWid, 24 * this.gridWid);
        g.setColor(Color.black.brighter());
        g.drawRect(0, 0, 10 * this.gridWid, 24 * this.gridWid);

        paintBoard(g);

        g.setFont(new Font("Arial",Font.BOLD,scoreSize));
        g.setColor(Color.YELLOW);
        g.drawString("NEXT:",10*this.gridWid+20,3*scoreSize);
        this.currentP.NextPieceSize = 4*scoreSize;
        this.currentP.setNewPieceWell = bNewPieceSetWell;

        if (currentP != null) {
            this.currentP.setGridWid(this.gridWid);
            this.currentP.paint(g);
        }
        paintGame1Start(g);
        paintGame1Result(g);
        paintGame2Start(g);
        paintGame2Result(g);
        paintPaused(g);
        paintScoreBoard(g);
    }
}
