package tetris;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 * Created by bwbecker on 2016-09-19.
 */

/**
 * Modified by Yiduo Jing on 2016-09-24.
 */

public class TetrisMain {

    public static void main(String[] args) {
        try {
            SplashScreen splash = new SplashScreen();
            splash.displaySplash();

            ProgramArgs a = ProgramArgs.parseArgs(args);
            Tetris tetris = new Tetris(a.getFPS(), a.getSpeed(), a.getSequence());

            tetris.tetrisInit();


            JFrame frame = new JFrame("Tetris");
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame.setBackground(Color.black.brighter());

            frame.pack();
            frame.setSize(530,700);
            frame.setMinimumSize(new Dimension(480,667));
            frame.setMaximumSize(new Dimension(731,808));
            frame.setVisible(true);
            frame.setContentPane(tetris);


            frame.addKeyListener(new KeyAdapter() {

                public void keyPressed(KeyEvent e) {
                    if(!tetris.bGameStart) return;

                    int keycode = e.getKeyCode();

                    if(tetris.bIsPause && keycode != KeyEvent.VK_P) return;
                    switch (keycode) {
                        case KeyEvent.VK_4:
                        case KeyEvent.VK_LEFT:
                            tetris.moveLeft();
                            break;

                        case KeyEvent.VK_6:
                        case KeyEvent.VK_RIGHT:
                            tetris.moveRight();
                            break;

                        case KeyEvent.VK_8:
                        case KeyEvent.VK_SPACE:
                            tetris.pieceDrop();
                            break;

                        case KeyEvent.VK_UP:
                        case KeyEvent.VK_X:
                        case KeyEvent.VK_1:
                        case KeyEvent.VK_5:
                        case KeyEvent.VK_9:
                            tetris.rotate(1);
                            break;

                        case KeyEvent.VK_CONTROL:
                        case KeyEvent.VK_Z:
                        case KeyEvent.VK_3:
                        case KeyEvent.VK_7:
                            tetris.rotate(-1);
                            break;

                        case KeyEvent.VK_P:
                            tetris.Pause();
                            break;
                    }
                }
            });


            frame.addMouseListener(new MouseAdapter() {
                @Override
                public void mousePressed(MouseEvent e) {
                    if(!tetris.bGameStart || tetris.bIsPause) return;
                    tetris.mousePressed(e.getPoint());
                }
            });

            frame.addMouseMotionListener(new MouseMotionAdapter() {
                @Override
                public void mouseMoved(MouseEvent e) {
                    if(!tetris.bGameStart || tetris.bIsPause) return;
                    tetris.mouseMoved(e.getPoint());
                }
            });

            frame.addMouseWheelListener(new MouseWheelListener() {
                @Override
                public void mouseWheelMoved(MouseWheelEvent e) {
                    if(!tetris.bGameStart || tetris.bIsPause) return;
                    if(tetris.bPieceSelected)
                        tetris.rotate(e.getWheelRotation());
                }
            });

            frame.addComponentListener(new ComponentAdapter() {
                @Override
                public void componentResized(ComponentEvent e) {
                    if(!tetris.bGameStart) return;
                    if(tetris.bIsPause)
                    {
                        tetris.setCompSize(frame.getWidth(),frame.getHeight());
                    }
                }
            });


        } catch (IllegalArgumentException e) {
            System.out.println(e);
        }
    }
}


