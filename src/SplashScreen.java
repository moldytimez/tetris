package tetris;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Jessica on 9/29/16.
 */
public class SplashScreen extends JWindow {
   // JComponent splashGraph;

    public SplashScreen() {
        this.setSize(500, 500);
        this.setVisible(true);
        this.setLocationRelativeTo(null);

        JComponent splashGraph = (JComponent) this.getContentPane();
        splashGraph.setBackground(Color.cyan.darker());
        Graphics g = splashGraph.getGraphics();
        g.setColor(Color.white);
        g.setFont(new Font("Century Gothic",Font.BOLD,30));
        g.drawString("Tetris",200,50);
        g.setFont(new Font("Arial",Font.CENTER_BASELINE,13));
        g.drawLine(30,75,465,75);
        g.drawString("MoveLeft: Left Arrow | MousePress+MouseMoveLeft | Numpad 4",30,100);
        g.drawLine(30,115,465,115);
        g.drawString("MoveRight: Right Arrow | MousePress+MouseMoveRight | Numpad 6",30,140);
        g.drawLine(30,155,465,155);
        g.drawString("Drop: Space Bar | MousePress twice | Numpad 8",30,180);
        g.drawLine(30,195,465,195);
        g.drawString("RotateRight: Up Arrow, X | MousePress+MouseWheelUp | Numpad 1,5,9",30,220);
        g.drawLine(30,235,465,235);
        g.drawString("RotateLeft: Control, Z | MousePress+MouseWheelDown | Numpad 3,7",30,260);
        g.drawLine(30,275,465,275);
        g.drawString("Pause: P | Numpad 1,5,9",30,300);
        g.drawLine(30,315,465,315);

        g.setFont(new Font("Arial",Font.CENTER_BASELINE,13));
        g.drawString("Loading...",220,400);
    }

    public void displaySplash() {
        try
        { Thread.sleep(5000); }
        catch (Exception e) {
        }
        this.setVisible(false);
    }

}
