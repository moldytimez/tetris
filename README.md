Some feature:

1. The score rule depends on how many combos that one user can achieve at once and each combo worths 5 marks. For example, if user makes 1 row cleared, he will get 1 combo with 5 marks. If user makes 2 row cleared at once, he will get 10 marks for the 2 combos.

2. My Tetris has two levels of games. The first level requires user to score at 100 to pass it. If user does not get more than 100 at level one, he will lose the whole game. If he achieves the 100 goal scores, he will automatically get into the second level game. The second level game requires 250 scores. The user will win the game if he passes level 2, otherwise, he will lose the game.

3. The initial position for one piece to drop down is fixed at the third grid. Game over due to no piece can drop down from that position.}